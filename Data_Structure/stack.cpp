#include "stack.hpp"
using std::cout;
using std::endl;
using std::cerr;
using std::exception;

//logic error
using std::domain_error;
using std::invalid_argument;
using std::out_of_range;
using std::logic_error;

//runtime error
using std::range_error;
using std::runtime_error;

//anonymous namespace
namespace{

template<typename T>
void pop_and_display(e23::Stack<T>& srcStack ){
	while(!srcStack.isEmpty()){
		cout<<srcStack.pop()<<"->";
	}
	cout <<endl;
}

void linked_list_stack_test(){
	cout <<"<<Prepare to test Linked list Stack>>"<<endl;
	e23::Stack_L<int>* ptStackA = new e23::Stack_L<int>();

	//push data
	ptStackA->push(123);
#if 1 //used to test one node if flag=0
	for(int i=1;i<6;++i){
		ptStackA->push(i);
	}
#endif
	cout <<"test copy constructor"<<endl;
	e23::Stack_L<int> myStackB(*ptStackA);
	pop_and_display<int>(myStackB);

	cout <<"test assignment operator"<<endl;
	myStackB=*ptStackA;
	pop_and_display<int>(myStackB);
	myStackB=*ptStackA;

	cout <<"test destructor"<<endl;
	delete ptStackA;

	cout <<"<<Linked list Stack test finished>>"<<endl;
}

void array_stack_test(){

	cout <<"<<Prepare to test Array Stack>>"<<endl;
	e23::Stack_A<int>* ptArrayStack = new e23::Stack_A<int>(7);
	//push data
	ptArrayStack->push(123);
#if 1  //used to test one node if flag=0
	for(size_t i=0;i<7;++i){
		ptArrayStack->push(i);
	}
#endif
	cout <<"test copy constructor"<<endl;
	e23::Stack_A<int>  ArrayStack(*ptArrayStack);
	pop_and_display<int>(ArrayStack);
	cout <<"test assignment operator"<<endl;
	ArrayStack=*ptArrayStack;
	pop_and_display<int>(ArrayStack);

	cout <<"test destructor"<<endl;
	delete ptArrayStack;

	cout <<"<<Array Stack test finished>>"<<endl;
}


}//anonymous namespace


namespace e23{

void stack_test(){
	linked_list_stack_test();
	array_stack_test();
}


}
/*
int main (int argc,char** argv){
	try{
		stack_test();
	}
	catch(std::domain_error& domain_err){
		cerr <<domain_err.what()<<endl;
	}
	catch(std::invalid_argument& invalid_err){
		cerr <<invalid_err.what()<<endl;
	}
	catch(std::out_of_range& out_of_range_err){
		cerr <<out_of_range_err.what()<<endl;
	}
	catch(std::logic_error logic_err){
		cerr <<logic_err.what()<<endl;
	}
	catch(std::range_error& range_err){
		cerr <<range_err.what()<<endl;
	}
	catch(std::runtime_error runtime_err){
		cerr <<runtime_err.what();
	}
	catch(std::exception& except_err){
		cerr <<except_err.what()<<endl;
	}
	catch(...){
		cerr <<"unknown exception caught"<<endl;
	}

	exit (EXIT_SUCCESS);
}
*/
