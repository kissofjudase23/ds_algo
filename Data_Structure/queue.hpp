//let C++ can invoke C library correctly.
extern "C"{
#include <pthread.h>
#include "tlpi_hdr.h"
}
#include "my_exception.hpp"
#include <iostream>
#include <stdexcept>

#ifndef QUEUE_HPP
#define QUEUE_HPP

namespace e23 {

template<typename T> class Queue {
public:
	virtual bool isEmpty()=0;
	virtual void enQueue(const T&)=0;
	virtual T deQueue()=0;
protected:
	explicit Queue(){} //constructor
	virtual ~Queue() noexcept{} //destructor
};//Queue


//todo: templcate specialization for char* or const char* type
//due to the copy constructor and assignment issue
/*
	tmplate<> class Queue<const char*>{};
	tmplate<> class Queue<char*>{};
*/
//Queue_L(linked list verion):space complexity bigO(N)
template<typename T>
class Queue_L :public Queue<T> {
protected:
	class Node{
		public:
		/*if we use T instad of T*, T needs construcotr, copy constructor 
		and assignment operator*/
		T iTem;
		Node* pNext;
		//constructorA (invoke constctor of iTem with type T)
		explicit Node(const T& SrciTem):iTem(SrciTem),pNext(0){}
		//copy constructor( invoke constctor of iTem with type T)
		explicit Node(const Node& SrcNode):iTem(SrcNode.iTem),
		pNext(SrcNode.pNext){}
		//assignment operator
		Node& operator=(const Node& SrcNode){
			iTem=SrcNode.iTem;
			pNext=SrcNode.pNext;
			return *this;
		};
		//destructor
		~Node(){}
	};//Node
private:
	Node* pFront; //point to the front of the queue(for dequeue)
	Node* pRear;  //point to the rear of the queue (for enqueue)
	virtual void release();
	virtual void copy(const Queue_L<T>&);
public:
	explicit Queue_L():pFront(0),pRear(0){};//constructor
	explicit Queue_L(const Queue_L<T>&);  //copy constructor
	virtual ~Queue_L() noexcept; //destructor
	Queue_L<T>& operator=(const Queue_L<T>&); //assignment operator
	virtual bool isEmpty();
	virtual void enQueue(const T&);
	virtual T deQueue();
};//Queue_L

//release: time complexity bigO(N)
template<typename T>
void Queue_L<T>::release(){
	Node* removed_node=pFront;
	while(removed_node){
		pFront=pFront->pNext;
		delete removed_node;
		removed_node=pFront;
	}
 	pFront=pRear=0;
}

//copy: time complexity bigO(N)
template<typename T>
void Queue_L<T>::copy(const Queue_L<T>& srcQueue){
	pFront=pRear=0;
	//copy the Front Node
	Node* src_traverse_node=srcQueue.pFront;
	if(src_traverse_node){
		Node* new_node = new Node(src_traverse_node->iTem);
		pFront = new_node;
		if(src_traverse_node == srcQueue.pRear){//this is rear Node!
			pRear=pFront;
		}
	}
	else return;
	//copy rest Nodes
	Node* dst_traverse_node=pFront;
	src_traverse_node=(src_traverse_node->pNext);
	while(src_traverse_node){
		Node* new_node = new Node(src_traverse_node->iTem);
		dst_traverse_node->pNext=new_node;
		dst_traverse_node=(dst_traverse_node->pNext);
		if(src_traverse_node == srcQueue.pRear){//this is rear Node!
			pRear=dst_traverse_node;
		}
		src_traverse_node=src_traverse_node->pNext;
	}
}

//copy constructor: time complexity bigO(N)
template<typename T>
Queue_L<T>::Queue_L(const Queue_L<T>& srcQueue){
	copy(srcQueue);
}

//assignment operator: time complexity bigO(N)
template<typename T>
Queue_L<T>& Queue_L<T>::operator=(const Queue_L<T>& srcQueue){
	release();
	copy(srcQueue);
	return *this;
}

//destrucotre: time complexity bigO(N)
template<typename T>
Queue_L<T>::~Queue_L() noexcept{
	release();
}

//isEmpty: time complexity bigO(1)
template<typename T>
bool Queue_L<T>::isEmpty(){
	if(!pFront) return TRUE;
	else return FALSE;
}

//enQueue: time complexity bigO(1)
template<typename T>
void Queue_L<T>::enQueue(const T& srcItem){
	Node* new_node= new Node(srcItem);
	if(pRear){
		pRear=(pRear->pNext=new_node);
	}
	else{//first node
		pFront=pRear=new_node;
	}
}

//deQueue: time complexity bigO(1)
template<typename T>
T Queue_L<T>::deQueue(){
	if(!pFront){
		throw_msg("queue is empty");
	}
	Node* deQueue_node=pFront;
	pFront=pFront->pNext;
	if(deQueue_node == pRear){//check if this is the rear node.
		pRear=0;
	}
	T return_val(deQueue_node->iTem);
	delete deQueue_node;
	return return_val;
}


//Queue_L(circular array version1):space complexity bigO(N)
template<typename T>
class Queue_CA :public Queue<T> {
protected:
	bool isFull;
	int  front;
	int  rear;
	size_t size;
	T* ptQueue;
	virtual void release();
	virtual void copy(const Queue_CA<T>&);
public:
	explicit Queue_CA(size_t);//constructor
	explicit Queue_CA(const Queue_CA<T>&);  //copy constructor
	virtual ~Queue_CA() noexcept; //destructor
	Queue_CA<T>& operator=(const Queue_CA<T>&); //assignment operator
	virtual bool isEmpty();
	virtual void enQueue(const T&);
	virtual T deQueue();
};//Queue_L


//release: time complexity bigO(1)
template<typename T>
void Queue_CA<T>::release(){
	front=rear=-1;
	size=0;
	isFull=0;
	if(ptQueue){
		delete []ptQueue;
	}
	 ptQueue=0;
}

//copy: time complexity: bigO(N)
template<typename T>
void Queue_CA<T>::copy(const Queue_CA<T>& srcQueue){
	front=srcQueue.front;
	rear=srcQueue.rear;
	size=srcQueue.size;
	isFull=srcQueue.isFull;
	if(size < 1){
		ptQueue=0;
		throw_msg("size of srcQueue should be greater than 1");
	}
	ptQueue = new T[size];
	for(int i=0; i < size; ++i){
		ptQueue[i]=srcQueue.ptQueue[i];
	}
}

//constructor: time complexity: bigO(1)
template<typename T>
Queue_CA<T>::Queue_CA(size_t array_size):isFull(0),front(0),rear(0),
size(array_size),ptQueue(0){
	if(size < 1){
		throw_msg("queue size must be greater than 1");
	}
	ptQueue = new T[size];
}

//copy constructor: time complexity bigO(N)
template<typename T>
Queue_CA<T>::Queue_CA(const Queue_CA<T>& srcQueue){
	copy(srcQueue);
}

//assignment operator: time complexity bigO(N)
template<typename T>
Queue_CA<T>& Queue_CA<T>::operator=(const Queue_CA& srcQueue){
	release();
	copy(srcQueue);
	return *this;
}

//destructor: time complexity bigO(1)
template<typename T>
Queue_CA<T>::~Queue_CA() noexcept{
	release();
}

//enQueue: time complexity bigO(1)
//use (size-1) only
template<typename T>
void Queue_CA<T>::enQueue(const T& srcItem){
	//the queue is full now
	if( (rear == front) && (isFull) ){
		throw_msg("the queue is full");
	}
	rear=(rear+1)%size;
	ptQueue[rear]=srcItem;
	//check if the queue is full now.
	if(rear == front){
		isFull=1;
	}
	return;
}

//deQueue; time complexity bigO(1)
template<typename T>
T Queue_CA<T>::deQueue(){
	//the queue is empty
	if( (front == rear) && (!isFull) ){
		throw_msg("the queue is empty now");
	}
	front=(front+1)%size;
	T return_val = ptQueue[front];
	//check if the queue is empty now
	if(front == rear){
		isFull=0;
	}
	 return return_val;
}
template<typename T>
bool Queue_CA<T>::isEmpty(){
	if( (front == rear) && (!isFull) ){
		return 1;
	}
	else{
		return 0;
	}
}

//provide test function to verify
void queue_test();

}//namespace e23




#endif



