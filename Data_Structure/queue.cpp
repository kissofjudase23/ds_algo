#include "queue.hpp"
using std::cout;
using std::endl;
using std::cerr;
using std::exception;

//logic error
using std::domain_error;
using std::invalid_argument;
using std::out_of_range;
using std::logic_error;

//runtime error
using std::range_error;
using std::runtime_error;

//anonymous namespace
namespace{

template<typename T>
void deQueue_and_display(e23::Queue<T>& srcQueue){
	while(!srcQueue.isEmpty()){
		cout <<srcQueue.deQueue()<<"->";
	}
	cout <<endl;
}

void linked_list_queue_test(){
	cout <<"Prepare to test Linked list Queue"<<endl;
	e23::Queue_L<int>* ptQueue_A= new e23::Queue_L<int>();
	for(int i=0; i<5; ++i){
		ptQueue_A->enQueue(i);
	}
	ptQueue_A->enQueue(123);

	cout <<"Test copy constructor"<<endl;
	e23::Queue_L<int>* ptQueue_B = new e23::Queue_L<int>(*ptQueue_A);
	deQueue_and_display<int>(*ptQueue_B);

	cout <<"Test assignment operator"<<endl;
	(*ptQueue_B)=(*ptQueue_A);
	deQueue_and_display<int>(*ptQueue_B);

	cout <<"test destrucotr"<<endl;
	delete ptQueue_A;
	delete ptQueue_B;
	cout <<"Linked list queue test finished"<<endl;
}

void circular_array_queue_test(){
	cout <<"Prepare to test circular array Queue"<<endl;
	e23::Queue_CA<int>* ptQueue_A= new e23::Queue_CA<int>(6);
	for(int i=0; i<6; ++i){
		ptQueue_A->enQueue(i);
	}
#if 0 
	ptQueue_A->enQueue(123);
#endif
	cout <<"Test copy constructor"<<endl;
	e23::Queue_CA<int>* ptQueue_B = new e23::Queue_CA<int>(*ptQueue_A);
	deQueue_and_display<int>(*ptQueue_B);

	cout <<"Test assignment operator"<<endl;
	(*ptQueue_B)=(*ptQueue_A);
	deQueue_and_display<int>(*ptQueue_B);

	cout <<"test destrucotr"<<endl;
	delete ptQueue_A;
	delete ptQueue_B;
	cout <<"Circular array queue test finished"<<endl;
}

}//anonymous





namespace e23{
	void queue_test(){
		linked_list_queue_test();
		circular_array_queue_test();
	}
}


/*
int main (int argc,char** argv){
	try{
		stack_test();
	}
	catch(std::domain_error& domain_err){
		cerr <<domain_err.what()<<endl;
	}
	catch(std::invalid_argument& invalid_err){
		cerr <<invalid_err.what()<<endl;
	}
	catch(std::out_of_range& out_of_range_err){
		cerr <<out_of_range_err.what()<<endl;
	}
	catch(std::logic_error logic_err){
		cerr <<logic_err.what()<<endl;
	}
	catch(std::range_error& range_err){
		cerr <<range_err.what()<<endl;
	}
	catch(std::runtime_error runtime_err){
		cerr <<runtime_err.what();
	}
	catch(std::exception& except_err){
		cerr <<except_err.what()<<endl;
	}
	catch(...){
		cerr <<"unknown exception caught"<<endl;
	}

	exit (EXIT_SUCCESS);
}
*/
