//let C++ can invoke C library correctly.
extern "C"{
#include <pthread.h>
#include "tlpi_hdr.h"
}
#include "my_exception.hpp"
#include <iostream>
#include <stdexcept>

#ifndef STACK_HPP
#define STACK_HPP

namespace e23 {

template<typename T> class Stack {
public:
	//Stack_L<T>& operator=(const Stack_L<T>&); //assignment operator
	virtual bool isEmpty()=0;
	virtual void push(const T&)=0;
	virtual T pop()=0;
protected:
	explicit Stack(){} //constructor
	virtual ~Stack() noexcept{} //destructor

};//Stack


//todo: templcate specialization for char* or const char* type
//due to the copy constructor and assignment issue
/*
	tmplate<> class Stack<const char*>{};
	tmplate<> class Stack<char*>{};
*/
//Stack(linked list verion):space complexity big O(N)
template<typename T>
class Stack_L :public Stack<T> {
protected:
	class Node{
		public:
		/*if we use T instad of T*, T needs construcotr, copy constructor 
		and assignment operator*/
		T iTem;
		Node* pNext;
		//constructorA (invoke constctor of iTem with type T)
		explicit Node(const T& SrciTem):iTem(SrciTem),pNext(0){}
		//copy constructor( invoke constctor of iTem with type T)
		explicit Node(const Node& SrcNode):iTem(SrcNode.iTem),
		pNext(SrcNode.pNext){}
		//assignment operator
		Node& operator=(const Node& SrcNode){
			iTem=SrcNode.iTem;
			pNext=SrcNode.pNext;
			return *this;
		};
		//destructor
		~Node(){}
	};//Node
private:
	Node* pTop; //point to the top node of the stack
	virtual void release();
	virtual void copy(const Stack_L<T>&);
public:
	explicit Stack_L();           //constructorA
	explicit Stack_L(const T&);   //constructorB
	explicit Stack_L(const Stack_L<T>&); //copy constructor
	virtual ~Stack_L() noexcept; //destructor
	Stack_L<T>& operator=(const Stack_L<T>&); //assignment operator
	virtual bool isEmpty();
	virtual void push(const T&);
	virtual T pop();
	virtual void traverse();
};//Stack

//constructorA:time complexity big O(1)
template<typename T>
Stack_L<T>::Stack_L():pTop(0){}

//constructorB:time complexity big O(1)
template<typename T>
Stack_L<T>::Stack_L(const T& srciTem){
	Node* new_node=new Node(srciTem);
	pTop=new_node;
}

//copy: time complexity big O(N)
template<typename T>
void Stack_L<T>::copy(const Stack_L<T>& srcStack){
	//copy top node first
	pTop=0;
	Node* src_traverse_node=srcStack.pTop;
	if(src_traverse_node){
		Node* new_node=new Node(src_traverse_node->iTem);
		this->pTop=new_node;
	}
	else return; //empty stack
	//copy rest node
	src_traverse_node=src_traverse_node->pNext;
	Node* dst_tail_node=pTop;
	while(src_traverse_node){
		Node* new_node=new Node(src_traverse_node->iTem);
		dst_tail_node->pNext=new_node;
		dst_tail_node=dst_tail_node->pNext;
		src_traverse_node=src_traverse_node->pNext;
	}
}

//copy constructor:time complexity big O(N)
template<typename T>
Stack_L<T>::Stack_L(const Stack_L<T>& srcStack){
	copy(srcStack);
}

//assignment operator:time complexity big O(N)
template<typename T>
Stack_L<T>& Stack_L<T>::operator= (const Stack_L<T>& srcStack){
	release();
	copy(srcStack);
	return *this;
}

//release:time complexity big O(N)
template<typename T>
void Stack_L<T>::release(){
	while(pTop){
		Node* removed_node=pTop;
		pTop=pTop->pNext;
		delete removed_node;
	}
	pTop=0;
}

template<typename T>
Stack_L<T>::~Stack_L() noexcept{
	release();
}

//push:time complexity big O(1)
template<typename T>
void Stack_L<T>::push(const T& srcItem){
	Node* new_node=new Node(srcItem);
	new_node->pNext=pTop;
	pTop=new_node;
}

//pop:time complexity big O(1)
template<typename T>
T Stack_L<T>::pop(){
	if(!pTop){
		throw_msg("stack is empty");
	}
	else{
		Node* pop_node=pTop;
		pTop=pTop->pNext;
		T return_val(pop_node->iTem);
		delete pop_node;
		return return_val;
	}
}

//isEmpty:time complexity bigO(1)
template<typename T>
bool Stack_L<T>::isEmpty(){
	if(!pTop) return TRUE;
	else return FALSE;
}

//traverse:time complexity big O(N)
template<typename T>
void Stack_L<T>::traverse(){
	Node* traverse_node=pTop; 
	while(traverse_node){
		std::cout <<traverse_node->iTem<<"->";
		traverse_node=traverse_node->pNext;
	}
	std::cout <<"NULL"<<std::endl;
}
void stack_test();


//Stack(Array_version):space complexity:big O(N)
template<typename T> 
class Stack_A:public Stack<T> {
private:
	int  top;    //point to the top node index of the stack
	size_t size;//array size
	T* ptStack;
	virtual void release();
	virtual void copy(const Stack_A<T>&);
public:
	explicit Stack_A(size_t);               //constructor
	explicit Stack_A(const Stack_A<T>&); //copy constructor
	virtual ~Stack_A() noexcept;                     //destructor
	Stack_A<T>& operator=(const Stack_A<T>&); //assignment operator
	virtual bool isEmpty();
	virtual void push(const T&);
	virtual T pop();
};//Stack

//constructor: time complexity big O(1)
template<typename T>
Stack_A<T>::Stack_A(size_t array_size):
top(-1),size(0),ptStack(0){
	if(array_size < 1){
		throw_msg("array size should be greater than 1");
	}
	size=array_size;
	ptStack = new T[size];
}

//release: time complexity big O(1)
template<typename T>
void Stack_A<T>::release(){
	top = -1;
	size = 0;
	if(ptStack){
		delete []ptStack;
	}
	ptStack = 0;
}

//destructor: time complexity big O(1)
template<typename T>
Stack_A<T>::~Stack_A<T>() noexcept{
	release();
}

//copy: time complexity big O(n)
template<typename T>
void Stack_A<T>::copy(const Stack_A<T>& srcStack){
	top=srcStack.top;
	size=srcStack.size;
	if(size < 1){
		throw_msg("Size of srcStack should be greater than 1");
	}
	ptStack=new T[size];
	for(size_t i=0;i<size;++i){
		ptStack[i]=srcStack.ptStack[i];
	}
}

//copy constructor: time complexity big O(n)
template<typename T>
Stack_A<T>::Stack_A(const Stack_A<T>& srcStack){
	copy(srcStack);
}

//assignment operator:time complexity big O(n)
template<typename T>
Stack_A<T>& Stack_A<T>::operator=(const Stack_A<T>& srcStack){
	release();
	copy(srcStack);
	return *this;
}

template<typename T>
bool Stack_A<T>::isEmpty(){
	if(top == -1) return 1;
	else return 0;
}

template<typename T>
void Stack_A<T>::push(const T& srcItem){
	if(top == (size-1)){
		std::cout <<"Stack is full"<<std::endl;
	}
	else{
		ptStack[++top]= srcItem;
	}
}

template<typename T>
T Stack_A<T>::pop(){
	if(top == -1){
		throw_msg("stack is empty");
	}
	return ptStack[top--];
}

}//namespace e23

#endif



