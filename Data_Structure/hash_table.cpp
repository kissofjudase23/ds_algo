#include "hash_table.hpp"

using std::cout;
using std::endl;
using std::cerr;
using std::exception;

//logic error
using std::domain_error;
using std::invalid_argument;
using std::out_of_range;
using std::logic_error;

//runtime error
using std::range_error;
using std::runtime_error;

//anonymous namespace
namespace{

void STL_hash_table_test(){
	cout <<"Hash Table(STL version) test starts!!\n\n";
	e23::HashTableST<std::string,int>* ptHashTable = (
			new e23::HashTableST<std::string,int>()
	);

	cout <<"push pair \"Tom 123\"\n";
	ptHashTable->put("Tom",123);
	cout <<"push pair \"Kason 456\"\n";
	ptHashTable->put("Kason",456);
	cout <<"push pair \"Fancy 789\"\n\n";
	ptHashTable->put("Fancy",789);
	ptHashTable->traverse();
	cout<<endl;

	cout <<"test copy constructor!\n";
	e23::HashTableST<std::string,int> HashTablecopy(*ptHashTable);
	HashTablecopy.traverse();
	cout<<endl;

	const int* resultVal= ptHashTable->get("Tom");
	if(resultVal){
		cout <<"find Tom in hash table,result is "<<(*resultVal)<<"\n";
	}else{
		cout <<"cannot find Tom in hash table"<<"\n";
	}
	resultVal=ptHashTable->get("Fanny");
	if(resultVal){
		cout <<"find Fanny in hash table,result is "<<(*resultVal)<<"\n";
	}else{
		cout <<"cannot find Fanny in hash table"<<"\n\n";
	}

	cout <<"Remove Kason from HashTable,result is "
		 <<ptHashTable->remove("Kason")<<"\n";
	cout <<"Remove Fanny from HashTable,result is "
		 <<ptHashTable->remove("Fanny")<<"\n\n";
	ptHashTable->traverse();
	cout<<endl;

	cout <<"test assignment operaotr!\n";
	(*ptHashTable) = HashTablecopy;
	ptHashTable->traverse();
	cout<<endl;

	cout <<"Test destructor\n";
	delete ptHashTable;

	cout <<"Hash Table(STL version) test finished!!\n";
}

}//anonymous

namespace e23{
	void hash_table_test(){
		STL_hash_table_test();
	}
}//e23


/*
int main (int argc,char** argv){
	try{
		stack_test();
	}
	catch(std::domain_error& domain_err){
		cerr <<domain_err.what()<<endl;
	}
	catch(std::invalid_argument& invalid_err){
		cerr <<invalid_err.what()<<endl;
	}
	catch(std::out_of_range& out_of_range_err){
		cerr <<out_of_range_err.what()<<endl;
	}
	catch(std::logic_error logic_err){
		cerr <<logic_err.what()<<endl;
	}
	catch(std::range_error& range_err){
		cerr <<range_err.what()<<endl;
	}
	catch(std::runtime_error runtime_err){
		cerr <<runtime_err.what();
	}
	catch(std::exception& except_err){
		cerr <<except_err.what()<<endl;
	}
	catch(...){
		cerr <<"unknown exception caught"<<endl;
	}

	exit (EXIT_SUCCESS);
}
*/
