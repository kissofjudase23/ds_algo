extern "C"{
#include <pthread.h>
#include "tlpi_hdr.h"
}
#include "my_exception.hpp"
#include "stack.hpp"
#include "linked_list.hpp"
#include "queue.hpp"
#include "tree.hpp"
#include "hash_table.hpp"

using std::cout;
using std::endl;
using std::cerr;
using std::exception;
//logic error
using std::domain_error;
using std::invalid_argument;
using std::out_of_range;
using std::logic_error;

//runtime error
using std::range_error;
using std::runtime_error;


namespace e23{}

int main (int argc,char** argv){
	try{
		//e23::hash_table_test();
		e23::tree_test();
		//e23::queue_test();
		//e23::stack_test();
		//e23::Linked_list_test(1); //option 0/1: single/double linked list
	}
	catch(std::domain_error& domain_err){
		cerr <<domain_err.what()<<endl;
	}
	catch(std::invalid_argument& invalid_err){
		cerr <<invalid_err.what()<<endl;
	}
	catch(std::out_of_range& out_of_range_err){
		cerr <<out_of_range_err.what()<<endl;
	}
	catch(e23::my_exception& my_ex){
		cerr <<my_ex.what()<<endl;
	}
	catch(std::logic_error& logic_err){
		cerr <<logic_err.what()<<endl;
	}
	catch(std::range_error& range_err){
		cerr <<range_err.what()<<endl;
	}
	catch(std::runtime_error& runtime_err){
		cerr <<runtime_err.what();
	}
	catch(std::exception& except_err){
		cerr <<except_err.what()<<endl;
	}
	catch(...){
		cerr <<"unknown exception caught"<<endl;
	}

	exit (EXIT_SUCCESS);
}
