//let C++ can invoke C library correctly.
extern "C"{
#include <pthread.h>
#include "tlpi_hdr.h"
}
#include "my_exception.hpp"
#include <iostream>
#include <stdexcept>
#include <map>
#include <algorithm>

#ifndef HASH_TABLE_HPP
#define HASH_TABLE_HPP

namespace e23 {

template<typename Tkey, typename Tval>
class HashEntry {
public:
	explicit HashEntry(Tkey srcKey,Tval srcVal):key(srcKey),val(srcVal){}
	explicit HashEntry(const HashEntry& srcHashEntry):key(srcHashEntry.key),
		val(srcHashEntry.val){}
	virtual ~HashEntry() noexcept{}
	virtual HashEntry& operator=(const HashEntry& srcHashEntry){
		key=srcHashEntry.key;
		val=srcHashEntry.val;
	}
	virtual Tkey getKey(){return key;}
	virtual Tval getVal(){return val;}
	virtual void setKey(Tkey srcKey){key=srcKey;}
	virtual void setVal(Tval srcVal){val=srcVal;}
protected:
	Tkey key;
	Tval val;
};//HashEntry


/* abstract base class template of hashtable
 * space complexity O(n) */
template<typename Tkey,typename Tval> 
class HashTable{
protected:
	explicit HashTable(){}
	virtual ~HashTable(){}
	virtual size_t hashFun(const Tkey&) const=0;
public:
	virtual void put(const Tkey&,const Tval&)=0;
	virtual const Tval* get(const Tkey&) const=0;
	virtual bool remove(const Tkey&)=0;
	virtual bool isEmpty() const=0;
};//HashTable


/* hashtableA (do not use STL)
 * space complexity O(n) */
template<typename Tkey,typename Tval>
class HashTableA: public HashTable<Tkey,Tval>{
protected:
	size_t  size;       //the number of the key pair in the table
	size_t  maxBktNum;  //the number of the buckets in the table
	typedef HashEntry<Tkey, Tval>* BUCKET;
	BUCKET* table;
	virtual size_t  hashFun(const Tkey&) const;
	//virtual void resize();
public:
 	HashTableA():HashTable<Tkey,Tval>(){}
	HashTableA(const HashTableA&);
	virtual ~HashTableA();
	HashTable<Tkey,Tval>& operator=(const HashTable<Tkey,Tval>&);
	HashTableA<Tkey,Tval>& operator=(const HashTableA<Tkey,Tval>&);
	virtual void put(const Tkey&,const Tval&);
	virtual const Tval* get(const Tkey&) const;
	virtual bool remove(const Tkey&);
	virtual bool isEmpty() const;
};//HashTable


/* hashtableST (implemented by STL)
 * space complexity O(n) */
#define default_bucket_num 256
template<typename Tkey,typename Tval> 
class HashTableST: public HashTable<Tkey,Tval>{
protected:
	size_t  size;     //the number of the key pair in the table
	size_t  maxBktNum;//the number of the buckets in the table
	typedef std::map<Tkey,Tval> BUCKET;
	typedef typename BUCKET::iterator BUCKET_IT;
	BUCKET* table;
	size_t  hashFun(const Tkey&) const;
	BUCKET& getBucket(const Tkey&) const;
	void release();
	void copy(const HashTableST<Tkey,Tval>&);
	//virtual void resize();
public:
	explicit HashTableST();
	explicit HashTableST(const HashTableST&);
	virtual ~HashTableST() noexcept;
	HashTable<Tkey,Tval>& operator=(const HashTable<Tkey,Tval>&);
	HashTableST<Tkey,Tval>& operator=(const HashTableST<Tkey,Tval>&);
	virtual void put(const Tkey&,const Tval&);
	virtual const Tval* get(const Tkey&) const;
	virtual bool remove(const Tkey&);
	virtual bool isEmpty() const;
	void traverse() const;
};//HashTableST


/* traverse: time complexity O(n) */
template<typename Tkey, typename Tval>
void HashTableST<Tkey,Tval>::traverse() const{
	std::cout <<"current table size is "<<size<<std::endl;
	std::cout <<"maxBktNum is "<<maxBktNum<<std::endl;
	for(size_t i=0; i < maxBktNum; ++i){
		BUCKET& bucket= table[i];
		if( bucket.size() > 0 ){
			std::cout <<"bucket["<<i<<"]->>";
			for(BUCKET_IT it = bucket.begin();it != bucket.end(); ++it){
				std::cout<<it->first<<":"<<it->second;
			}
			std::cout <<std::endl;
		}
	}
}

/* release: time complexity O(n) */
template<typename Tkey, typename Tval>
void HashTableST<Tkey,Tval>::release(){
	for(size_t i=0; i < maxBktNum; ++i){
		BUCKET& bucket= table[i];
		bucket.clear();
	}
	delete []table;
	table=0;
	maxBktNum=size=0;
}

/* copy: time complexity O(n) */
template<typename Tkey, typename Tval>
void HashTableST<Tkey,Tval>::copy(const HashTableST<Tkey,Tval>& srcHashTable){
	size = srcHashTable.size;
	maxBktNum = srcHashTable.maxBktNum;
	table = new BUCKET[maxBktNum];
	for(size_t i=0; i < maxBktNum; ++i){
		table[i] = srcHashTable.table[i];
	}
}

/* destructor: time complexity O(n) */
template<typename Tkey, typename Tval>
HashTableST<Tkey,Tval>::~HashTableST() noexcept{
	release();
}

/* assignment operator: time complexity O(n) */
template<typename Tkey,typename Tval>
HashTable<Tkey,Tval>& HashTableST<Tkey,Tval>::operator=
	(const HashTable<Tkey,Tval>& srcHashTable){
	return operator=(
		dynamic_cast<HashTableST<Tkey,Tval>&>(
			const_cast<HashTable<Tkey,Tval>&>(srcHashTable)
		)
	);
}

/* assignment operator: time complexity O(n) */
template<typename Tkey,typename Tval>
HashTableST<Tkey,Tval>& HashTableST<Tkey,Tval>::operator=
	(const HashTableST<Tkey,Tval>& srcHashTable){
	release();
	copy(srcHashTable);
	return *this;
}

/* constructor: time complexity O(1) */
template<typename Tkey, typename Tval>
HashTableST<Tkey,Tval>::HashTableST():
	size(0),
	maxBktNum(default_bucket_num),
	table(new BUCKET[maxBktNum]){}

/* copy constructor: time complexity O(n) */
template<typename Tkey, typename Tval>
HashTableST<Tkey,Tval>::HashTableST(const HashTableST& srcHashTable){
	copy(srcHashTable);
}

/* hash function: time compolexity O(1)?? */
template<typename Tkey, typename Tval>
size_t HashTableST<Tkey,Tval>::hashFun(const Tkey& key) const{
	std::hash<Tkey> key_hash;
	return ( (key_hash(key)) % maxBktNum );
}

/* get bucket: time complexity O(1) */
template<typename Tkey,typename Tval>
typename HashTableST<Tkey,Tval>::BUCKET& HashTableST<Tkey,Tval>::getBucket
	(const Tkey& key) const{
	size_t bucket_num = hashFun(key);
	if(bucket_num > maxBktNum){
		throw_msg("error return of hash function!");
	}
	return table[bucket_num];
}

/* put time complexity O(log(s)) (std::map is implemented by BST)
 * s:the size of the bucket */
template<typename Tkey, typename Tval>
void HashTableST<Tkey,Tval>::put(const Tkey& key,const Tval& val){
	BUCKET& bucket=getBucket(key); 
	BUCKET_IT it=bucket.find(key);
	if(it == bucket.end()){
		bucket.insert(bucket.begin(),std::pair<Tkey,Tval>(key,val));
		++size;
	}
	else{ //just update it.
		it->second=val;
 	}
}

/* get: time complexity O(log(s)) (std::map is implemented by BST)
 * s: the size of the bucket
 * return null if do not find the key */
template<typename Tkey,typename Tval>
const Tval* HashTableST<Tkey,Tval>::get(const Tkey& key) const{
	BUCKET& bucket=getBucket(key);
	BUCKET_IT it=bucket.find(key);
	if(it != bucket.end()){
		return &(it->second);
	}
	else{
		std::cout<<"do not find the key in the table\n";
		return 0;
	}
}

/* remove: time complexity O(log(s)) (std::map is implemented by BST)
 * s: the size of the bucket */
template<typename Tkey,typename Tval>
bool HashTableST<Tkey,Tval>::remove(const Tkey& key){
	BUCKET& bucket=getBucket(key);
	size_t iRet = bucket.erase(key);
	if(iRet > 0){
		size = size - iRet;
		if(size < 0){
			throw_msg("error table size calculation");
		}
		else{
			return 1;
		}
	}
	else{//iRet=0
		std::cout<<"do not find the key in the table\n";
		return 0;
	}
}

/* isEmpty: time complexity O(1) */
template<typename Tkey,typename Tval>
bool HashTableST<Tkey,Tval>::isEmpty() const{
	return (!size) ? 1 : 0 ;
}

void hash_table_test();


}//namespace e23




#endif



