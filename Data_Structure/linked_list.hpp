//let C++ can invoke C library correctly.
extern "C"{
#include <pthread.h>
#include "tlpi_hdr.h"
}
#include <my_exception.hpp>
#include <iostream>
#include <stdexcept>

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

namespace e23 {

class LinkedList {

	protected:
		virtual ~LinkedList(){}
		explicit LinkedList():list_head(0),list_tail(0){}
		//basic data node
		class Node{
			public:
				int val;
				Node* next;
				Node* prev;
				explicit Node(int Srcval):val(Srcval),next(0),prev(0){}
				//copy constructor
				explicit Node(const Node& SrcNode):val(SrcNode.val),
				next(SrcNode.next),prev(SrcNode.prev){}
				//assignment operator
				Node& operator=(const Node& SrcNode){
					val=SrcNode.val;
					next=SrcNode.next;
					prev=SrcNode.prev;
					return *this;
				}
				~Node(){}
		};
		Node* list_head;
		Node* list_tail;

	public:
		virtual int getLength() const=0;
		virtual void insertToTail(const int&)=0;
		virtual void insertToHead(const int&)=0;
		virtual void traverse() const=0;
		virtual void remove(const int&)=0;
		virtual void release()=0;
		virtual void reverse()=0;
};

//space complexity O(n)
class SingleLinkedList: public LinkedList{
	protected:
		virtual void copy(const SingleLinkedList&);
	public:
		virtual ~SingleLinkedList();
		explicit SingleLinkedList();
		//copy constructor
		explicit SingleLinkedList(const SingleLinkedList&);
		//assignment operator
		virtual SingleLinkedList& operator=(const SingleLinkedList&);
		virtual int getLength() const;
		virtual void insertToTail(const int&);
		virtual void insertToHead(const int&);
		virtual void traverse() const;
		virtual void remove(const int&);
		virtual void release();
		virtual void reverse();
};

//space complexity O(n)
class DoubleLinkedList: public SingleLinkedList{
	public:
		//getLength , traverse and release do not need to implement.
		virtual ~DoubleLinkedList();
		explicit DoubleLinkedList();
		//copy constructor
		explicit DoubleLinkedList(const DoubleLinkedList&);
		//assignment operator
		virtual SingleLinkedList& operator=(const SingleLinkedList&);
		virtual DoubleLinkedList& operator=(const DoubleLinkedList&);
		//virtual int getLength() const;
		virtual void insertToTail(const int&);
		virtual void insertToHead(const int&);
		//virtual void traverse() const;
		virtual void remove(const int&);
		//do not need to implement
		//virtual void release();
		virtual void reverse();
};


void Linked_list_test(const unsigned int& option);

}//end of name spacee23



#endif



