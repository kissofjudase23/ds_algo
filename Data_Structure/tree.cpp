#include "tree.hpp"

using std::cout;
using std::endl;
using std::cerr;
using std::exception;

//logic error
using std::domain_error;
using std::invalid_argument;
using std::out_of_range;
using std::logic_error;

//runtime error
using std::range_error;
using std::runtime_error;

//anonymous namespace
namespace{

template<typename T>
void print_tree_info(e23::Tree<T>& srcTree){
	cout <<"inorder of the tree is ";
	srcTree.traverse();
	cout <<"The tree height is "<<srcTree.getHeight()<<endl;
	cout <<"The number of nodes in the tree is "<<srcTree.getCount()<<endl;
}

void BST_tree_test(){
	cout <<"Binary seach tree test starts!!"<<endl;

	e23::BSTree<int>* ptBSTree = new e23::BSTree<int>();
	cout <<"add 7,13,5,1,6,11,15 to the tree"<<endl;
	ptBSTree->add(7);
	ptBSTree->add(13);
	ptBSTree->add(5);
	ptBSTree->add(1);
	ptBSTree->add(6);
	ptBSTree->add(11);
	ptBSTree->add(15);
	print_tree_info(*ptBSTree);

	cout <<"try to find 7 in BSTree, result is "<<ptBSTree->find(7)<<endl;
	cout <<"try to find 1 in BSTree, result is "<<ptBSTree->find(1)<<endl;
	cout <<"try to find 15 in BSTree, result is "<<ptBSTree->find(11)<<endl;
	cout <<"try to find 23 in BSTree, result is "<<ptBSTree->find(23)<<endl;

	cout <<"test copy contrcutor"<<endl;
	e23::BSTree<int>* ptBSTreeB = new e23::BSTree<int>(*ptBSTree);
	print_tree_info(*ptBSTreeB);

	cout <<"try to remove 7,11,13,999 From BSTree"<<endl;
	ptBSTreeB->remove(7);
	ptBSTreeB->remove(11);
	ptBSTreeB->remove(13);
	ptBSTreeB->remove(999);
	print_tree_info(*ptBSTreeB);
	cout <<"try to remove 1,5,6,15 From BSTree"<<endl;
	ptBSTreeB->remove(1);
	ptBSTreeB->remove(5);
	ptBSTreeB->remove(6);
	ptBSTreeB->remove(15);
	print_tree_info(*ptBSTreeB);

	cout <<"test assignmet operator"<<endl;
	*ptBSTreeB=*ptBSTree;
	print_tree_info(*ptBSTreeB);\

	cout <<"test invert operator"<<endl;
	ptBSTreeB->invert();
	print_tree_info(*ptBSTreeB);

	cout <<"test destrucotr!"<<endl;
	delete ptBSTree;
	delete ptBSTreeB;
	cout <<"Binary seach tree test finished!!"<<endl;
}

}//end of anonymous 

namespace e23{

void tree_test(){
	BST_tree_test();
}

}//end of namespace e23

