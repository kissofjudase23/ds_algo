#include "linked_list.hpp"

using std::cout;
using std::endl;
using std::cerr;
using std::exception;

//logic error
using std::domain_error;
using std::invalid_argument;
using std::out_of_range;
using std::logic_error;
//runtime error
using std::range_error;
using std::runtime_error;


namespace e23{

SingleLinkedList::SingleLinkedList():LinkedList(){}

SingleLinkedList::~SingleLinkedList(){
	release();
}

void SingleLinkedList::copy(const SingleLinkedList& srcList){
	list_head=list_tail=0;
	Node* traverse_node=srcList.list_head;
	while(traverse_node){
		this->insertToTail(traverse_node->val);
		traverse_node=traverse_node->next;
	}
}

//Time complexity: big O(n)
//new object
SingleLinkedList::SingleLinkedList(const SingleLinkedList& srcList){
	copy(srcList);
}

//Time complexity: big O(n)
//existing object
SingleLinkedList& SingleLinkedList::operator=(const SingleLinkedList& srcList){
	release();
	copy(srcList);
	return *this;
}


//Time complexity: big O(n)
int SingleLinkedList::getLength() const{
	unsigned int len=0;
	const Node* traverse_node= list_head;
	while(traverse_node){
		len++;
		traverse_node=traverse_node->next;
	}
	return len;
}

//Time complexity: big O(n)
void SingleLinkedList::traverse() const{
	if(!list_head){
		cout <<"this is an empty list"<<endl;
		return;
	}

	const Node* traverse_node= list_head;
	while(traverse_node){
		cout <<traverse_node->val<<"->";
		traverse_node=traverse_node->next;
	}
	cout <<endl<<"list_head="<<list_head->val<<endl;
	cout <<"list_tail="<<list_tail->val<<endl;
}

//Two list pointers (head and tail): Time complexity: O(1)
//One list pointer  (head) ->O(1)) : Time complexity: O(n)
void SingleLinkedList::insertToTail(const int& val){
	Node* new_node= new Node(val);
	//this is the first node
	if(!list_head){
		list_head=list_tail=new_node;
		return;
	}
	else{
		list_tail->next=new_node;
		list_tail=new_node;
	}
}

//Time complexity big O(1)
void SingleLinkedList::insertToHead(const int& val){
	Node* new_node= new Node(val);
	new_node->next=list_head;
	list_head = new_node;
	//this is the first node.
	if(!list_tail) list_tail=new_node;
}

//Time complexity big O(n)
void SingleLinkedList::release(){
	Node* removed_node=list_head;
	while(removed_node){
		list_head=list_head->next;
		delete removed_node;
		removed_node=list_head;
	}
	list_head=list_tail=0;
}

//Time complexity big O(N)
void SingleLinkedList::remove(const int& remove_target){

	if(!list_head){
		cout <<"this is an empty list"<<endl;
		return;
	}
	Node* prev=0; //important, use pseudo prev node.
	Node* trav=list_head;
	while(trav){
		if(trav->val == remove_target){
			Node* removed_node = trav;
			if(prev){
				prev->next=trav->next;
			}
			trav=trav->next;
			delete removed_node;
			if (list_tail && list_tail == removed_node){
				list_tail=0;
			}
			if (list_head && list_head == removed_node){
				list_head=0;
			}
		}
		else{
			prev=trav;
			trav=trav->next;
		}
	}
	/*
	//case1: target is head node.
	while( list_head && (list_head->val == remove_target) ){
		Node* removed_node = list_head;
		list_head = list_head->next;
		//check if it is the tail node.
		if (list_tail && list_tail == removed_node){
			list_tail=0;
		}
		delete removed_node;
	}
	//case2: target is not head node. (traver_node->next is check target)
	//user prev can be more clear, but need anotehr pointer.
	Node* traverse_node=list_head;
	while( traverse_node && (traverse_node->next) ){
		if( (traverse_node->next)->val == remove_target){
			//point current node to next next node.
			Node* removed_node = traverse_node->next;
			//this implies forward
			traverse_node->next=removed_node->next;
			if (list_tail && list_tail == removed_node){
				list_tail=0;
			}
			delete removed_node;
		}
		else traverse_node=traverse_node->next;
	}
	*/
}

void SingleLinkedList::reverse(){
	if(!list_head) return;
	
	//assumet prev point to a pseduo previous node NULL
	Node* prev=0,*current=list_head;
	list_tail=list_head;

	//at least two nodes.
	while(current){
		//record the next node
		Node* next=current->next;
		//point to prev node
		current->next=prev;
		//update prev and current
		prev=current;
		current=next;
	}

	//point new head to old tail.
	if(prev)
		list_head=prev;
}

//Time complexity big O(N)
DoubleLinkedList::~DoubleLinkedList(){
	SingleLinkedList::release();
}
DoubleLinkedList::DoubleLinkedList():SingleLinkedList(){}

//Time complexity: big O(n)
DoubleLinkedList::DoubleLinkedList(const DoubleLinkedList& srcList)
:SingleLinkedList(srcList){
}

//override assignment operator from SinglLinkedList
SingleLinkedList& DoubleLinkedList::operator=(const SingleLinkedList& srcList){
	//use the assignment operator below.
	this->DoubleLinkedList::operator=
	(dynamic_cast<DoubleLinkedList&>(const_cast<SingleLinkedList&>(srcList)));
	return *this;
} 

//Time complexity: big O(n)
DoubleLinkedList& DoubleLinkedList::operator=(const DoubleLinkedList& srcList){
	//reuse the operator in single linkest list 
	//it should be refine later?
	this->SingleLinkedList::operator=(srcList);
	return *this;
}

//Time complexity big O(1)
void DoubleLinkedList::insertToTail(const int& val){
	cout <<"val="<<val<<endl;
	Node* new_node= new Node(val);
	//this is the first node
	if(!list_head){
		list_head=list_tail=new_node;
		return;
	}
	else{
		list_tail->next=new_node;
		new_node->prev=list_tail;
		list_tail=new_node;
	}
}

//Time complexity big O(1)
void DoubleLinkedList::insertToHead(const int& val){
	Node* new_node= new Node(val);
	//this is the first node
	if(!list_head){
		list_head=list_tail=new_node;
		return;
	}
	else{
		list_head->prev=new_node;
		new_node->next=list_head;
		list_head=new_node;
	}
}

//Time complexity big O(N)
void DoubleLinkedList::remove(const int& remove_target){
	if(!list_head){
		cout <<"this is an empty list"<<endl;
		return;
	}
	Node* prev=0; //important, use pseudo prev node.
	Node* trav=list_head;
	while(trav){
		if(trav->val == remove_target){
			Node* removed_node = trav;
			if(prev){
				prev->next=trav->next;
				if(trav->next){
					trav->next = prev;
				}
			}
			trav=trav->next;
			delete removed_node;
			if (list_tail && list_tail == removed_node){
				list_tail=0;
			}
			if (list_head && list_head == removed_node){
				list_head=0;
			}
		}
		else{
			prev=trav;
			trav=trav->next;
		}
	}

	/*
	//case1: target is head node.
	while( list_head && (list_head->val == remove_target) ){
		Node* removed_node = list_head;
		list_head = list_head->next;
		if(list_head){
			list_head->prev=0;
		}
		//check if it is the tail node.
		if (list_tail && list_tail == removed_node){
			list_tail=0;
		}
		delete removed_node;
	}
	//case2: target is not head node.
	Node* traverse_node=list_head;
	while( traverse_node && (traverse_node->next) ){
		if( (traverse_node->next)->val == remove_target){
			//point current node to next next node.
			Node* removed_node = traverse_node->next;
			//this implies forward
			traverse_node->next=removed_node->next;
			if(traverse_node->next){
				(traverse_node->next)->prev=traverse_node;
			}
			if (list_tail && list_tail == removed_node){
				list_tail=0;
			}
			delete removed_node;
		}
		else traverse_node=traverse_node->next;
	}
	*/
}

//Time complexity big O(1)
void DoubleLinkedList::reverse(){
	if(!list_head) return;
	Node* traverse_node=list_head;
	while(traverse_node){
		std::swap<Node*>(traverse_node->next,traverse_node->prev);
		//new previous node is old next node.
		traverse_node=traverse_node->prev;
	}
	std::swap<Node*>(list_head,list_tail);
}

void Linked_list_test(const unsigned int& option){

	e23::SingleLinkedList* pList=0;
	switch(option){
		case 0:
			cout <<"<<single linked list test start>>"<<endl;
			pList =new e23::SingleLinkedList();
			break;
		case 1:
			cout <<"<<double linked list tset start>>"<<endl;
			pList = static_cast<e23::SingleLinkedList*>
					(new e23::DoubleLinkedList());
			break;
		default:
			throw_msg("unknown option");
			break;
	}
	//create a list like 1->5->5->5->5->1->NULL
	for(int i=0; i<4; i++){
		int temp=5;
		pList->insertToTail(temp);
	}
	pList->insertToHead(1);
	pList->insertToTail(1);
	cout <<"insert 1 5 5 5 1 to the list"<<endl;
	pList->traverse();

	//remove node with value5 from list.
	cout <<"<<remove 5 from list>>"<<endl;
	pList->remove(5);
	pList->traverse();
	//remove node with value1 from list.
	cout <<"<<remove 1 from list>>"<<endl;
	pList->remove(1);
	pList->traverse();
	
	for(int i=0; i<4; i++){
		pList->insertToTail(i);
	}
	pList->traverse();
	cout <<"<<test reverse functio>>n"<<endl;
	pList->reverse();
	pList->traverse();

	if(option == 1){
		cout <<"<<test copy constructor>>"<<endl;
		e23::DoubleLinkedList* pDListCopy 
		= new e23::DoubleLinkedList(dynamic_cast<e23::DoubleLinkedList&>(*pList));
		pDListCopy->traverse();
		cout <<"<<test assignment operator>>"<<endl;
		pDListCopy = new e23::DoubleLinkedList();
		pDListCopy->operator=(dynamic_cast<e23::DoubleLinkedList&>(*pList));
		delete pDListCopy;
	}

	delete pList;
	cout <<"<<LinkedList test finished>>"<<endl;
}

}//end of namespace e23

/*
int main (int argc,char** argv){

	try{
		//Linked_list_test(0);
		Linked_list_test(1);
	}
	catch(std::domain_error& domain_err){
		cerr <<domain_err.what()<<endl;
	}
	catch(std::invalid_argument& invalid_err){
		cerr <<invalid_err.what()<<endl;
	}
	catch(std::out_of_range& out_of_range_err){
		cerr <<out_of_range_err.what()<<endl;
	}
	catch(std::range_error& range_err){
		cerr <<range_err.what()<<endl;
	}
	catch(std::exception& except_err){
		cerr <<except_err.what()<<endl;
	}
	catch(...){
		cerr <<"unknown exception caught"<<endl;
	}

	exit (EXIT_SUCCESS);
}
*/
