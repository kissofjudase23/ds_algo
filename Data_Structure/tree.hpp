//let C++ can invoke C library correctly.
extern "C"{
#include <pthread.h>
#include "tlpi_hdr.h"
}
#include "my_exception.hpp"
#include <iostream>
#include <stdexcept>
#include <vector>
#include <cmath>

#ifndef TREE_HPP
#define TREE_HPP

namespace e23 {

template<typename T>
class TreeNode{
protected:
	TreeNode(){};
	virtual ~TreeNode() noexcept{};
public:
	virtual void add(const T&)=0;
	virtual TreeNode<T>* remove(const T&,TreeNode<T>*)=0;
	virtual bool find(const T&) const=0;
	virtual size_t  getHeight() const=0;//Get the tree height from this Node.
	virtual size_t  getCount() const=0; //Get the number of tree from this Node.
	virtual void traverse() const=0;    //Traverse from this Node.
};//Tree Node


//Binary search Tree: space complexity O(N)
template<typename T>
class BSTNode :public TreeNode<T>{
private:
	T val;
	BSTNode* pLeft;
	BSTNode* pRight;
	T findMin()const;
	T findMax()const;
public:
	explicit BSTNode(const T& rVal):val(rVal),pLeft(0),pRight(0){}
	explicit BSTNode(const BSTNode<T>& rNode):val(rNode.val),pLeft(0),pRight(0)
	{}
	virtual ~BSTNode() noexcept{};
	BSTNode& operator=(const BSTNode<T>& rNode){
		val=rNode.val;
		pLeft=pRight=0;
		return *this;
	}
	virtual void releaseAll();
	virtual void copyAll(const BSTNode<T>&);
	virtual void add(const T&);
	virtual TreeNode<T>* remove(const T&,TreeNode<T>*);
	virtual BSTNode<T>*  remove(const T&,BSTNode<T>*);
	virtual BSTNode<T>*  invert();
	virtual bool find(const T&) const;
	virtual size_t  getHeight() const; //Get the tree height
	virtual size_t  getCount() const;  //Get the number of nodes of the ree.
	virtual void traverse() const; //Traverse this tree.
};//BSTNode

//releaseAll: time complexity O(N)
template<typename T>
void BSTNode<T>::releaseAll(){
	if(pLeft){
		pLeft->releaseAll();
		delete pLeft;
		pLeft=0;
	}
	if(pRight){
		pRight->releaseAll();
		delete pRight;
		pRight=0;
	}
}

//copyAll:time complexity O(N)
template<typename T>
void BSTNode<T>::copyAll(const BSTNode<T>& srcNode){
	if(srcNode.pLeft){
		pLeft=new BSTNode<T>( *(srcNode.pLeft) );
		pLeft->copyAll( *(srcNode.pLeft));
	}
	if(srcNode.pRight){
		pRight=new BSTNode<T>( *(srcNode.pRight) );
		pRight->copyAll( *(srcNode.pRight) );
	}
}

//add: time complexity O(log(N))
//notes: do not allow duplicate node.
template<typename T> 
void BSTNode<T>::add(const T& srcVal){
	if(srcVal > val){
		if(!pRight){
			pRight = new BSTNode(srcVal);
		}
		else{
			pRight->add(srcVal);
		}
	}
	else if(srcVal <val){ 
 		if(!pLeft){
			pLeft = new BSTNode(srcVal);
		}
		else{
			pLeft->add(srcVal);
		}
	}
	else{//srcVal == val
		std::cout<<"The srcVal is duplicated"<<std::endl;
		//throw_msg("duplicate val");
	}
}

//override the TreeNode::remove inherited from TreeNode
template<typename T>
TreeNode<T>* BSTNode<T>::remove(const T& srcVal,TreeNode<T>* parent){
	return remove(srcVal,dynamic_cast<BSTNode<T>*>(parent));
}

/*
 * remove: time complexity O(log(N))
 * If parent == NULL,this is Root node.
 * return NULL if do not find.
 *
 * There are three cases when remove a node from a BST tree.
 * case1:the node has two children.
 * case2:the node has only one child.
 * case3:the node has no children.
 * http://www.algolist.net/Data_structures/Binary_search_tree/Removal
*/ 
template<typename T>
BSTNode<T>* BSTNode<T>::remove(const T& removeVal,BSTNode<T>* parent){
	if(removeVal > val){
		return ((!pRight) ? NULL:(pRight->remove(removeVal,this)));
	}
	else if(removeVal < val){
		return ((!pLeft) ? (NULL):(pLeft->remove(removeVal,this)));
	}
	else{//removeVal == val
		if(pLeft){
			//replace val by the maximum value in the left subtree.
			//notice that min/max value should be leaf node!
			val=pLeft->findMax();
			//delete the maximum value in the left subtree
			return pLeft->remove(val,this);
		}
		else if(pRight){
			 //replace val by the minimum value in the right subtree.
			val=pRight->findMin();
			//delete the minimum value in the right subtree.
			return pRight->remove(val,this);
		}
		else{//leaf Node (no children)
			if(parent){
				if(this == parent->pRight){
					parent->pRight = NULL;
				}
				else if(this == parent->pLeft){
					parent->pLeft = NULL;
				}
				else{
					throw_msg("This is not parent Node!!");
				}
			}
			return this;
		}
	}
}

/*invert O(n)*/
template<typename T>
BSTNode<T>* BSTNode<T>::invert(){
	BSTNode<T>* tmpLeft=pLeft;
	pLeft = (pRight) ? pRight->invert():0;
	pRight = (tmpLeft) ? tmpLeft->invert():0;
	return this;
}

//find Min:time complexity O(log(N))
//notice that Min value should be leaf node.
template<typename T>
T BSTNode<T>::findMin() const{
	return ((!pLeft) ? (val):(pLeft->findMin()));
}

//find Max:time complexity O(log(N))
//notice that Max value should be leaf node.
template<typename T>
T BSTNode<T>::findMax() const{
	return ((!pRight) ? (val):(pRight->findMax()));
}

//find: time complexity O(log(N))
template<typename T>
bool BSTNode<T>::find(const T& target) const{
	if(target > val){
		return ( (!pRight) ? (0):(pRight->find(target)) );
	}
	else if(target < val){
		return ((!pLeft) ? (0):(pLeft->find(target)) );
	}
	else{//srcVal == val
		return 1;
	}
}

//traverse: time complexity O(N)
//notes: inorder
template<typename T>
void BSTNode<T>::traverse() const{
	if(pLeft){
		pLeft->traverse();
	}
	std::cout<<val<<"->";
	if(pRight){
		pRight->traverse();
	 }
}

//getHeight: time complexity O(N)
template<typename T>
size_t BSTNode<T>::getHeight() const{
	size_t leftTreeHeight=0,rightTreeHeight=0;
	if(pLeft){
		leftTreeHeight=pLeft->getHeight();
	}
	if(pRight){
		rightTreeHeight=pRight->getHeight();
	}
	return 1+(std::max<size_t>(leftTreeHeight,rightTreeHeight));
}

//getCount: time complexity O(N)
template<typename T>
size_t BSTNode<T>::getCount() const{
	size_t leftCount=0,rightCount=0;
	if(pLeft){
		leftCount=pLeft->getCount();
	}
	if(pRight){
		rightCount=pRight->getCount();
	}
	return leftCount+rightCount+1;
}

template<typename T> class Tree {
public:
	virtual void add(const T&)=0;
	virtual void remove(const T&)=0;
	virtual bool find(const T&) const=0;
	virtual bool isEmpty() const=0;
	virtual size_t  getHeight() const=0; //Get the tree height
	virtual size_t  getCount() const=0;  //Get the number of nodes of the ree.
	virtual void traverse() const=0; //Traverse this tree.
	virtual void invert() =0; //Invert the tree
protected:
	explicit Tree(){}          //constructor
	virtual ~Tree() noexcept{} //destructor
};//Tree

/*
* todo:
* 1.templcate specialization for char* or const char* type
*	tmplate<> class BSTree<const char*>{};
*	tmplate<> class BST<char*>{};
* 2.AVL tree (to avoid worst case of O(N))
*/
//BSTree(Binary Search Tree):space complexity O(N)
template<typename T>
class BSTree :public Tree<T> {
protected:
private:
	BSTNode<T>* pRoot; //point to the front of the tree
	void release();
	void copy(const BSTree<T>&);
public:
	virtual void add(const T&);
	virtual void remove(const T&);
	virtual bool find(const T&) const;
	virtual bool isEmpty() const;
	virtual size_t  getHeight() const;
	virtual size_t  getCount() const;
	void traverse() const;
	void invert() ;
	explicit BSTree():pRoot(0){}        //constructor
	explicit BSTree(const BSTree<T>&);  //copy constructor
	virtual ~BSTree() noexcept;         //destructor
	BSTree<T>& operator=(const BSTree<T>&); //assignment operator
};//BSTree

//release: Time complexity O(N)
template<typename T>
void BSTree<T>::release(){
	 if(pRoot){ 
		pRoot->releaseAll();
		pRoot=0;
	 }
}

//copy: Time complexity O(N)
template<typename T>
void BSTree<T>::copy(const BSTree<T>& srcTree){
	if(!srcTree.pRoot){
		std::cout<<"The source BSTree is an empty tree!!"<<std::endl;
		pRoot=0;
		return;
	}
	pRoot = new BSTNode<T>(*(srcTree.pRoot));
	pRoot->copyAll(*(srcTree.pRoot));
}

//destrucotre: Time complexity O(N)
template<typename T>
BSTree<T>::~BSTree() noexcept{
	release();
}

//copy constructor: Time complexity O(N)
template<typename T>
BSTree<T>::BSTree(const BSTree<T>& srcTree){
	copy(srcTree);
}

//assignment operator:Time complexity O(N)
template<typename T>
BSTree<T>& BSTree<T>::operator=(const BSTree<T>& srcTree){
	release();
	copy(srcTree);
	return *this;
}

//add: Time complexity O(log(N))
template<typename T>
void BSTree<T>::add(const T& val){
	if (!pRoot){
		pRoot = new BSTNode<T>(val);
		return;
	}
	pRoot->add(val);
}

//remove: Time complexity O(log(N))
template<typename T>
void BSTree<T>::remove(const T& removeVal){
	if(!pRoot){
		std::cout<<"The tree is empty!"<<std::endl;
		return;
	}
	BSTNode<T>* sudoParent=0;
	BSTNode<T>* remove_node = pRoot->remove(removeVal,sudoParent);
	if(remove_node){
		if(remove_node == pRoot){
			pRoot=0;
		}
		delete remove_node;
	}
}

//isEmpty: Time complexity O(1)
template<typename T>
bool BSTree<T>::isEmpty() const{
	return (!pRoot) ? 1:0;
}

//getHeight: Time complexity O(N)
template<typename T>
size_t BSTree<T>::getHeight() const{
	return ( (!pRoot) ? (0):(pRoot->getHeight()) );
}

//getCount: Time complexity O(N)
template<typename T>
size_t BSTree<T>::getCount() const{
	return ((!pRoot) ? (0):(pRoot->getCount()) );
}

//traverse: time complexity O(N)
template<typename T>
void BSTree<T>::traverse() const{
	if(!pRoot){
		std::cout<<"This is an empty BST tree"<<std::endl;
		return;
	}
	pRoot->traverse();
	std::cout<<std::endl;
}

//invert: O(n)
template<typename T>
void BSTree<T>::invert() {
	if(!pRoot){
		std::cout<<"This is an empty BST tree"<<std::endl;
		return;
	}
	pRoot->invert();
}

//find: time complexity O(log(N))
template<typename T>
bool BSTree<T>::find(const T& target) const{
	if(!pRoot){
		std::cout<<"The BST tree is empty"<<std::endl;
		return 0;
	}
	return pRoot->find(target);
}

void tree_test();


/* MinHeap is a complete binaray tree
 * */
template<typename T> class MinHeap {
private:
	std::vector<T> heap;
	int getParent(const size_t&);
	int getLeftChild(const size_t&);
	int getRightChild(const size_t&);
	void heapifyUp(const size_t&);
	void heapifyDown(const size_t&);
	void release();
	void copy();
public:
	T extractMin();
	virtual void add(const T&);
	virtual void remove(const T&);
	virtual bool find(const T&) const;
	virtual bool isEmpty() const;
	virtual size_t  getHeight() const; //Get the tree height
	virtual size_t  getCount() const;  //Get the number of nodes of the ree.
	virtual void traverse() const;     //Traverse this tree.
	virtual void invert() ;          //Invert the tree
protected:
	explicit MinHeap();          //constructor
	virtual ~MinHeap() noexcept; //destructor
};//MinHeap

//constructor: O(1)
template<typename T> 
MinHeap<T>::MinHeap(){
	heap.reserve(10);
}

template<typename T>
bool MinHeap<T>::isEmpty() const{
	return (!heap.size()) ? 1:0;
}

template<typename T>
size_t MinHeap<T>::getCount() const{
	return heap.size();
}

/* get height: O(1)
 * since heap is complete binary tree,
 * H = ceiling( log(N)+1 )* */
template<typename T>
size_t MinHeap<T>::getHeight() const{
	if (heap.size() > 0){
		return ceil(log2(heap.size()+1));
	}
	else{//size == 0
		0;
	}
}

/* get index of parent : O(1)*/
template<typename T>
int MinHeap<T>::getParent(const size_t& childIdx){
	if (childIdx > 0){
		return (childIdx-1)/2;
	}
	return -1;//no parent
}

/* get index of left child : O(1)*/
template<typename T>
int MinHeap<T>::getLeftChild(const size_t& parentIdx){
	int lChildIdx = (parentIdx*2) + 1;
	if (lChildIdx < heap.size()){
		return lChildIdx;
	}
	//out of scope
	return -1; 
}

/* get index of right child : O(1)*/
template<typename T>
int MinHeap<T>::getRightChild(const size_t& parentIdx){
	int rChildIdx = (parentIdx*2) + 2;
	if (rChildIdx < heap.size()){
		return rChildIdx;
	}
	//out of scope
	return -1;
}

/* heapUp: O(log(n) */
template<typename T>
void MinHeap<T>::heapifyUp(const size_t& childIdx){
	int parentIdx = getParent(childIdx);
	if(childIdx > 0 && parentIdx >= 0 && (heap[childIdx] < heap[parentIdx])){
		std::swap<T>(heap[childIdx],heap[parentIdx]);
		heapifyUp(parentIdx);
	}
}

/* heapDown: O(log(n) */
template<typename T>
void MinHeap<T>::heapifyDown(const size_t& parentIdx){

	int lChildIdx  = getLeftChild(parentIdx);
	int rChildIdx = getRightChild(parentIdx);
	int childIdx = lChildIdx;
	
	if(lChildIdx > 0 && rChildIdx > 0 && (heap[rChildIdx] < heap[lChildIdx]) ){
		childIdx = rChildIdx;
	}
	if(childIdx > 0 && heap[childIdx] < heap[parentIdx]){
		std::swap<T>( heap[childIdx],heap[parentIdx] );
		heapifyDown(childIdx);
	}
}

/* add: O(n) */
template<typename T>
void MinHeap<T>::add(const T& val){
	heap.push_back(val);
	heapifyUp(heap.size()-1);
}

/* extractMin: O(log(n))*/
template<typename T>
T MinHeap<T>::extractMin(){
	size_t size = heap.size();
	if (size == 0){
		std::cout<<"the min heap is empty"<<std::endl;
	}
	std::swap<T>(heap[0],heap[size-1]);
	T rVal = heap.pop_back();
	heapifyDown(0);
	return rVal;
}



}//namespace e23




#endif



