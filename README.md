# Description
<pre>
 This project is used to implement data structures and algorithms
 [Data Structure]
	 1.Single Linked List,Double Linkedt List
	 2.Linked List Stack and Array Stack (class template version)
	 3.Linked List Queue and Circular Array Queue (class template version)
	 4.Binary Search Tree (class template version)
	 5.Simple Hash Table (class template version implemented by std::map)
	 7.AVL Tree (to do)
	 8.Red Black Tree (to do)
	 6.Heap Tree (only implement add and extractMin)
 [Algorithm]
	1.Binary Search 
	2.Insertion Sort
	3.Selection Sort
	4.Bubble Sort
	5.Quick Sort 
	6.Merge Sort
	7.Heap Sort (use Min-Heap to verify this algorithm)
	8.Radix Sort (to do)
</pre>

# Build code
<pre><code>$ cd $work_dir/ds_algo
$ make
</code></pre>

# Clean 
<pre><code>$ cd $work_dir/ds_algo
$ make clean
</code></pre> 
