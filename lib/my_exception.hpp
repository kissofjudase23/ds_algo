#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#ifndef MY_EXCEPTION_HPP
#define MY_EXCEPTION_HPP

namespace e23{

class my_exception: public std::logic_error {
private:
	std::string msg;
public:
	my_exception(const std::string &arg, const char *file, const char *func,
	int line)noexcept:std::logic_error(arg) {
		std::ostringstream o;
		o <<"file:"<<file << " func:" << func<< " line:"<< line << " msg:" << arg;
		msg= o.str();
	}
	~my_exception() noexcept{}
	virtual const char *what() const noexcept{
		return msg.c_str();
	}
};

}//namespace e23

#define throw_msg(arg) throw e23::my_exception(arg, __FILE__, __func__ , __LINE__);
#endif



