#!/bin/bash
# author:Tom
# Date:2016/01/20

#DIR_LIST=( "Algorithm" "Data_Structure" "miscellaneous" "lib")
DIR_LIST=( "${PWD}" )

function Generate_DB_File() {
	echo "<<Start to generate database for ctags and cscope!>>"
	#create ctag data base
	for dir in ${DIR_LIST[@]}
	do
		cd ${dir}
		#create ctags db files
		ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .
		#create cscope db files
		find . -name "*.h"\
		-o -name "*.hpp"  \
		-o -name "*.c"    \
		-o -name "*.cpp"  \
		-o -name "*.py"   \
		 >cscope.files
		cscope -Rbkq -i cscope.files
	cd -
	done
	echo "<<Finished!>>"
}

function Clean_DB_File() {
	echo "<<Clean cTag and cscope database files>>"
	#create ctag data base
	for dir in ${DIR_LIST[@]}
	do
		cd $dir
		rm tags cscope*
		cd -
	done
	echo "<<Clean database Finished!>>"
}


function main() {
	echo "option is \"${1}\""
	case "${1}" in
		make)
		Generate_DB_File
		;;
		clean)
		Clean_DB_File
		;;
		*)
		echo "usage example ${0} {make/clean}"
		;;
	esac
}

option=${1}
main ${option}




