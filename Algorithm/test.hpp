#include <iostream>

#ifndef TEST_H
#define TEST_H

namespace e23{

template<typename T> void def_test(T);

template void def_test<int>(int);

}//e23

#include "test_impl.hpp"
#endif
