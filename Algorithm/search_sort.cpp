#include "search_sort.hpp"
#include <algorithm>

using std::cout;
using std::endl;
using std::cerr;
using std::exception;

//logic error
using std::domain_error;
using std::invalid_argument;
using std::out_of_range;
using std::logic_error;

//runtime error
using std::range_error;
using std::runtime_error;


//anonymous namespace
namespace{

typedef std::vector<int> VEC_INT;
typedef std::vector<VEC_INT> VEC_INT_ARRAY;

typedef enum sort_option{
	insertion,
	selection,
	bubble,
	quick,
	merge
}SORT_OPTION;

typedef enum search_option{
	binary,
}Search_OPTION;

void prepare_test_case(VEC_INT_ARRAY& vecArray){
	//test cases
	size_t num_case=10;
	vecArray.reserve(num_case);
	VEC_INT special_caseA = {500,500,500,500,500,500,500};
	VEC_INT special_caseB = {0,0,0,0,0,0,0};
	VEC_INT special_caseC = {200,200};
	VEC_INT special_caseD = {100};
	VEC_INT special_caseE = {7,6};
	VEC_INT descending_case = {10,9,8,7,6,5,4,3,2,1};
	VEC_INT ascending_case = {1,2,3,4,5,6,7,8,9,10};
	VEC_INT random_case = {1,5,11,19,15,26,59,61,48,37};
	vecArray.push_back(special_caseE);
	vecArray.push_back(special_caseA);
	vecArray.push_back(special_caseB);
	vecArray.push_back(special_caseC);
	vecArray.push_back(special_caseD);
	vecArray.push_back(special_caseE);
	vecArray.push_back(descending_case);
	vecArray.push_back(ascending_case);
	vecArray.push_back(random_case);
}

void select_your_sorting_algo(const size_t& option, const bool& verbose){
	//test cases array
	VEC_INT_ARRAY vecArray;
	prepare_test_case(vecArray);
	VEC_INT_ARRAY verifyArray;
	verifyArray=vecArray;
	size_t eleNum = vecArray.size();
	//start sort
	for(size_t i=0; i < eleNum; ++i){
		switch(option){
			case insertion:
				e23::insertion_sort(vecArray[i],verbose);
				break;
			case selection:
				e23::selection_sort(vecArray[i],verbose);
				break;
			case bubble:
				e23::bubble_sort(vecArray[i],verbose);
				break;
			case quick:
				e23::quick_sort(vecArray[i],verbose);
				break;
			case merge:
				e23::merge_sort(vecArray[i],verbose);
				break;
			default:
				cout <<"unknown option"<<endl;
				break;
		}//switch
		//use this list to verify.
		std::sort(verifyArray[i].begin(),verifyArray[i].end());
		if (std::equal(verifyArray[i].begin(),verifyArray[i].end(),
				vecArray[i].begin())){
			cout <<"verify ok!: ";
			print_vec(verifyArray[i]);
		}
		else{
			cerr <<"verify failed, the answer shoud be: ";
			throw_msg("something woring in your sorting algorithm!");
			print_vec(verifyArray[i]);
		}
		cout << endl;
	}//for
}

void select_your_search_algo(const size_t& option){
	//test cases array
	//start sort
	VEC_INT test_caseA = {1,5,11,15,19,26,37,48,59,61};
	switch(option){
		case 0:
			e23::binary_search(test_caseA,1,0,test_caseA.size()-1);
			e23::binary_search(test_caseA,5,0,test_caseA.size()-1);
			e23::binary_search(test_caseA,11,0,test_caseA.size()-1);
			e23::binary_search(test_caseA,37,0,test_caseA.size()-1);
			e23::binary_search(test_caseA,48,0,test_caseA.size()-1);
			e23::binary_search(test_caseA,61,0,test_caseA.size()-1);
			e23::binary_search(test_caseA,9,0,test_caseA.size()-1);
			break;
		default:
				cout <<"unknown option"<<endl;
				break;
	}//switch
	cout << endl;
}


}//anonymous



namespace e23{
	void sort_test(){
		bool normalVerbose=0;
		bool testVerbose=1;
		select_your_sorting_algo(insertion,normalVerbose);
		select_your_sorting_algo(selection,normalVerbose);
		select_your_sorting_algo(bubble,normalVerbose);
		select_your_sorting_algo(quick,normalVerbose);
		select_your_sorting_algo(merge,testVerbose);
	}
	
	void search_test(){
		//select_your_search_algo(binary);
	}
	

}//e23


/*
int main (int argc,char** argv){
	try{
		stack_test();
	}
	catch(std::domain_error& domain_err){
		cerr <<domain_err.what()<<endl;
	}
	catch(std::invalid_argument& invalid_err){
		cerr <<invalid_err.what()<<endl;
	}
	catch(std::out_of_range& out_of_range_err){
		cerr <<out_of_range_err.what()<<endl;
	}
	catch(std::logic_error logic_err){
		cerr <<logic_err.what()<<endl;
	}
	catch(std::range_error& range_err){
		cerr <<range_err.what()<<endl;
	}
	catch(std::runtime_error runtime_err){
		cerr <<runtime_err.what();
	}
	catch(std::exception& except_err){
		cerr <<except_err.what()<<endl;
	}
	catch(...){
		cerr <<"unknown exception caught"<<endl;
	}

	exit (EXIT_SUCCESS);
}
*/
