//let C++ can invoke C library correctly.
extern "C"{
#include <pthread.h>
#include "tlpi_hdr.h"
}
#include "my_exception.hpp"
#include <iostream>
#include <stdexcept>
#include <vector>


#ifndef SORT_HPP
#define SORT_HPP

namespace{
/* print all elements in the vector
 * O(n) */
template<typename Tval>
void print_vec(std::vector<Tval>& vec){
	typename std::vector<Tval>::iterator it;
	std::cout <<"Vector contains:";
	for(it = vec.begin(); it != vec.end(); ++it){
		std::cout<<" "<<*it;
	}
	std::cout<<std::endl;
}

template<typename Tval>
void insert_to_sorted_array(std::vector<Tval>& vec,size_t insertIdx){
	Tval insertVal= vec[insertIdx];
	while( (insertIdx > 0) && (insertVal < vec[insertIdx-1])){
		//shift right the element in [insertIdx-1].
		vec[insertIdx] = vec[insertIdx - 1];
		--insertIdx;
	}
	vec[insertIdx] = insertVal;
}

}//anonnymous

namespace e23 {

/* binary search: O(n)
 * T(n)=T(n/2)+1
 * */
template<typename Tval>
bool binary_search(std::vector<Tval>& vec,Tval target,
		const int left,const int right){
	size_t vecSize= vec.size();
	if(vecSize < 1){
		std::cout <<"this is an empty vec"<<std::endl;
		return 0;
	}
	/*notes: equal is necessary*/
	if(left <= right){
		int mid=(left+right)/2;
		if(target < vec[mid]){
			//find the target in the left part
			return binary_search(vec,target,left,mid-1);
		}
		if(target > vec[mid]){
			//find the target in the right part
			return binary_search(vec,target,mid+1,right);
		}
		//finded!
		else{
			std::cout <<"try to find "<<target<<" in the sorted vec:"<<std::endl;
			print_vec(vec);
			std::cout <<"find it:)"<<std::endl;
			return 1;
		}
	}
	std::cout <<"try to find "<<target<<" in the sorted vec:"<<std::endl;
	print_vec(vec);
	std::cout <<"do not find it 0rz"<<std::endl;
	return 0;
}

/* insertion sort
 * best:  O(n)   ascending series
 * worst: O(n^2) descending series
 * avg:   O(n^2)*/
template<typename Tval>
void insertion_sort(std::vector<Tval>& vec, const bool& verbose){
	std::string sortName="insertion";
	std::cout <<"<<before "<<sortName<<" sort ";
	print_vec(vec);
	size_t vecSize= vec.size();
	if(vecSize >= 2){
		/* i from index(1) to index(size-1), total (size-1) rounds.
		 * insert the element in the position i to the
		 * "sorted array" which containes i elements (from 0 to i-1)*/
		for(size_t i = 1;i < vecSize;++i){
			insert_to_sorted_array<Tval>(vec,i);
			if(verbose){
				print_vec(vec);
			}
		}
	}
 	std::cout <<"<<after "<<sortName<<" sort ";
	print_vec(vec);
}

/* selection sort
 * best/avg/worst case: O(n^2) */
template<typename Tval>
void selection_sort(std::vector<Tval>& vec,const bool& verbose){
	std::string sortName="selection";
	std::cout <<"<<before "<<sortName<<" sort ";
	print_vec(vec);
	size_t vecSize= vec.size();
	if(vecSize >= 2){
		/* find the ith min element in the list.
		 * need (n-1) rounds */
		for(size_t i = 0; i <= (vecSize-2); ++i){
			Tval minIdx = i;
			for(size_t j=i+1; j <= (vecSize-1); ++j){
				if (vec[j] < vec[minIdx]){
					minIdx = j;
				}
			}
			if(minIdx != i){
				std::swap<Tval>(vec[minIdx],vec[i]);
			}
			if(verbose){
				print_vec(vec);
			}
		}
	}
 	std::cout <<"<<after "<<sortName<<" sort ";
	print_vec(vec);
}

/* bubble sort
 * best:  O(n)    ascending series
 * worst: O(n^2) descending series
 * avg:   O(n^2) */
template<typename Tval>
void bubble_sort(std::vector<Tval>& vec,const bool& verbose){
	std::string sortName="bubble"; 
	std::cout <<"<<before "<<sortName<<" sort ";
	print_vec(vec);
	size_t vecSize= vec.size();
	if(vecSize >= 2){
		//total (n-1) rounds
		for( size_t i=1; i <= (vecSize-1); ++i){
			bool bFin=0;
			for (size_t j=0; j <= ((vecSize-1)-i); j++){
				if(vec[j] > vec[j+1]){
					std::swap<Tval>(vec[j],vec[j+1]);
					bFin = 1;
				}
			}
			if(verbose){
				print_vec(vec);
			}
			//there is no swap occurs!
			if(!bFin){
				break;
			}
		}
	}
	std::cout <<"<<after "<<sortName<<" sort ";
	print_vec(vec);
}

/*recursion */
template<typename Tval>
void quick_sort_partition(std::vector<Tval>& vec,
						const int left,
						const int right,
						const bool& verbose){
	int bFromLeft = left;
	int lFromRight = right;
	int pivotIdx = (left+right)/2;
	Tval pivot = vec[pivotIdx];
	/* why equal is needed here?
	*  since the equal is only happened when 
	*  vec[bFromLeft]=pivot=vec[bFromRight]. 
	*  That is, pivot key do not need to be included in the sublists needing to
	*  to be sorted later.*/
	while(bFromLeft <= lFromRight){
		/* find the value is greater than or equal to pivot from left,
		 * equal is necessary to prevent infinite loop ex. 7 7 7 7 7 */
		while(vec[bFromLeft] < pivot){
			++bFromLeft;
		}
		while(vec[lFromRight] > pivot){
			--lFromRight;
		}
		if(bFromLeft <= lFromRight){
			//do not need swap if bFromLeft == lFromRight
			if(bFromLeft < lFromRight){
				std::swap<Tval>(vec[bFromLeft],vec[lFromRight]);
			}
			++bFromLeft;
			--lFromRight;
		}
	}
	if(verbose){
		print_vec(vec);
	}
	//left part, this check is necessary.
	if(lFromRight > left)
		quick_sort_partition(vec,left,lFromRight,verbose);
	//right part
	if(bFromLeft < right)
		quick_sort_partition(vec,bFromLeft,right,verbose);
}

/* quick sort
 * the time complexity is determined by the num of recursive.
 *	best:  O(nlog(n))
 *  worst: O(N^2) pivot is the min/max value in the list.
 *  avg:   O(nlog(n)) 
 *  space complexitY: determined by num of recursion
 *  O(log(n)~O(n)
 *  */
template<typename Tval>
void quick_sort(std::vector<Tval>& vec,const bool& verbose){
	std::string sortName="quick";
	std::cout <<"<<before "<<sortName<<" sort ";
	print_vec(vec);
	size_t vecSize= vec.size();
	if(vecSize >= 2){
		quick_sort_partition<Tval>(vec,0,vecSize-1,verbose);
	}
	std::cout <<"<<after "<<sortName<<" sort ";
	print_vec(vec);
}

template<typename Tval>
void merge_partition(std::vector<Tval>& vec,std::vector<Tval>& helpVec,
		const size_t& left, const size_t& mid, const size_t& right,
		const bool& verbose){

	/* copy both halves into a help vec */
	for(size_t i = left;i <= right; ++i){
		helpVec[i]=vec[i];
	}
	size_t helpLeft=left;
	size_t helpRight=mid+1;
	size_t vecCurrent=left;
	
	/* copying back the smaller element from the two halves into the 
	 * original vec */
	while( helpLeft <= mid && helpRight <= right){
		/* == here is used for stable sort 
		copy back for left run */
		if(helpVec[helpLeft] <= helpVec[helpRight]){
			vec[vecCurrent]=helpVec[helpLeft];
			++helpLeft;
		}
		else{/*copy back from right run */
			vec[vecCurrent]=helpVec[helpRight];
			++helpRight;
		}
		++vecCurrent;
	}
	/*either left run or right run has been completely sacned*/
	if(helpLeft <= mid){
		size_t leftRemain = (mid - helpLeft + 1);
		for( size_t i=1; i <= leftRemain; ++i){
			vec[vecCurrent++]=helpVec[helpLeft++];
		}
	}
	/* The right half do not need to copy even hepRight < right
	* since it's already there */
}


template<typename Tval>
void merge_sort_partition(std::vector<Tval>& vec,std::vector<Tval>& helpVec,
		const size_t& left, const size_t& right, const bool& verbose){
	if(left < right){
		size_t mid=(left + right)/2;
		//sort the left partition
		merge_sort_partition(vec,helpVec,left,mid,0);
		if(verbose){
			std::cout <<"left part finished"<<std::endl;
			print_vec(vec);
		}
		//sort the right partition
		merge_sort_partition(vec,helpVec,mid+1,right,0);
		if(verbose){
			std::cout <<"right part finished"<<std::endl;
			print_vec(vec);
		}
		//the merge part does all the heavy lifting
		merge_partition(vec,helpVec,left,mid,right,verbose);
	}
}

/* mrege sort
 * the time complexity is determined by the num of recursive.
 * best/worst/avg:  O(nlog(n))
 * space complexitY: determined by the external space 
 * O(n)
 * */
template<typename Tval>
void merge_sort(std::vector<Tval>& vec,const bool& verbose){
	std::string sortName="merge";
	std::cout <<"<<before "<<sortName<<" sort ";
	print_vec(vec);
	size_t vecSize= vec.size();
	if(vecSize >= 2){
		std::vector<Tval> helpVec;
		helpVec.resize(vecSize);
		merge_sort_partition<Tval>(vec,helpVec,0,vecSize-1,verbose);
	}
	std::cout <<"<<after "<<sortName<<" sort ";
	print_vec(vec);
}

void search_test();
void sort_test();

}//namespace e23




#endif



