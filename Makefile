# Makefile to build all programs in all subdirectories
#
# DIRS is a list of all subdirectories containing makefiles
# (The library directory is first so that the library gets built first)
#

DIRS = lib \
	Miscellaneous \
	Data_Structure \
	Algorithm

# The "namespaces" directory is deliberately excluded from the above
# list because much of the code requires a fairly recent kernel and
# userspace to build. Nevertheless, there is a Makefile in that directory.

BUILD_DIRS = ${DIRS} ${CDIRS}

# Dummy targets for building and clobbering everything in all subdirectories

all: 	
	bash gen_ctags_cscope_db.sh make
	@ for dir in ${BUILD_DIRS}; do (cd $${dir}; ${MAKE}) ; done

allgen: 
	bash gen_ctags_cscope_db.sh make
	@ for dir in ${BUILD_DIRS}; do (cd $${dir}; ${MAKE} allgen) ; done

clean: 
	bash gen_ctags_cscope_db.sh clean
	@ for dir in ${BUILD_DIRS}; do (cd $${dir}; ${MAKE} clean) ; done

showvariable:
	@ echo ${MAKE}
	@ echo ${BUILD_DIRS}
